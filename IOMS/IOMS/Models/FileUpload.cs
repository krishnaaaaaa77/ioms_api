﻿namespace IOMS.Models
{
    public class UploadFile
    {
        public int id 
        {
        get; set; }
        public int SubjectID
        {
            get; set;
        }

        public string TeacherName
        {
            get; set;
        }

        public string FileURL
        {
            get; set;
        }

        public string FileName
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }
    }
}
