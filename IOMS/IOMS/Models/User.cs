﻿namespace IOMS.Models
{
    public class UserModel
    {
        public int ID
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
        public string UserName
        {
            get; set;
        }
        public string Email
        {
            get; set;
        }
        public string? Password
        {
            get; set;
        }
        public int RoleID
        {
            get; set;
        } = 6;

        public bool isDeleted 
        {
            get; set;
        } = false;
    }
}
