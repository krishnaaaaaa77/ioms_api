﻿namespace IOMS.Models
{
    public class UserIdSubject
    {
        public int Id
        {
            get; set;
        }

        public int UserID
        {
            get; set;
        }

        public int SubjectId
        {
            get; set;
        }
    }

    public class UserAssignSubject
    {
        public int UserID
        {
            get; set;
        }

        public int[] SubjectIds
        {
            get; set;
        }
    }
}
