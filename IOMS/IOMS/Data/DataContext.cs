﻿using IOMS.Models;
using Microsoft.EntityFrameworkCore;

namespace IOMS.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> Options) : base(Options) { }

        public DbSet<RolesModel> Roles
        {
            get; set;
        }
        public DbSet<UserModel> User
        {
            get; set;
        }
        public DbSet<Subject> Subject
        {
            get; set;
        }
        public DbSet<UserIdSubject> UserIdSubject
        {
            get; set;
        }
        public DbSet<UploadFile> UploadFile
        {
            get; set;
        }
    }
}
