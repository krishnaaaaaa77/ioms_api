﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IOMS.Migrations
{
    public partial class UploadFile1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "UploadFile",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "UploadFile");
        }
    }
}
