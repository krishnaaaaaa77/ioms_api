﻿using IOMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using IOMS.Data;

namespace IOMS.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly DataContext _dataContext;
        public UserController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        [HttpGet("getUsers")]
        public async Task<ActionResult<List<UserModel>>> Get()
        {
            var user = await _dataContext.User.Where(u=>u.RoleID!=1 && u.RoleID != 0 && u.isDeleted==false).ToListAsync();
            return Ok(user);
        }

        [HttpGet("getUser/{id}")]
        public async Task<ActionResult<List<UserModel>>> GetByID(int id)
        {
            var user = await _dataContext.User.Where(u=>u.ID==id).FirstAsync();
            if (user == null)
            {
                return BadRequest("User not found");
            }
            return Ok(user);
        }

        [HttpPost("createUser")]
        public async Task<ActionResult<List<UserModel>>> AddUser(UserModel User)
        {
            _dataContext.User.Add(User);
            await _dataContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("updateUser")]
        public async Task<ActionResult<List<UserModel>>> UpdateUser(UserModel User)
        {
            var user = await _dataContext.User.FindAsync(User.ID);
            if (user == null)
            {
                return BadRequest("User not found");
            }
            user.Name = User.Name;
            user.UserName = User.UserName;
            user.RoleID = User.RoleID;
            user.Email = User.Email;

            await _dataContext.SaveChangesAsync();
            return Ok();
        }


        [HttpDelete("deleteUser/{id}")]
        public async Task<ActionResult<List<UserModel>>> DeleteUser(int id)
        {
            var user = await _dataContext.User.FindAsync(id);
            if (user == null)
            {
                return BadRequest("User not found");
            }
            user.isDeleted = true;
            await _dataContext.SaveChangesAsync();
            return Ok();
        }
    }
}
