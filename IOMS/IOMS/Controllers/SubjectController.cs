﻿using IOMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using IOMS.Data;

namespace IOMS.Controllers
{
    [Route("api/Subject")]
    [ApiController]
    public class SubjectController : Controller
    {
        private readonly DataContext _dataContext;
        public SubjectController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        [HttpGet("getSubjects")]
        public async Task<ActionResult<List<Subject>>> Get()
        {
            var subjects = await _dataContext.Subject.Where(u=>u.IsDeleted== false).ToListAsync();
            return Ok(subjects);
        }

        [HttpGet("getSubject/{id}")]
        public async Task<ActionResult<List<Subject>>> GetByID(int id)
        {
            var subject = await _dataContext.Subject.Where(s => s.Id == id).FirstAsync();
            if (subject == null)
            {
                return BadRequest("subject not found");
            }
            return Ok(subject);
        }

        [HttpPost("createSubject")]
        public async Task<ActionResult<List<Subject>>> AddSubject(Subject sub)
        {
            _dataContext.Subject.Add(sub);
            await _dataContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("updateSubject")]
        public async Task<ActionResult<List<Subject>>> UpdateSubject(Subject sub)
        {
            var subject = await _dataContext.Subject.FindAsync(sub.Id);
            if (subject == null)
            {
                return BadRequest("subject not found");
            }
            subject.Name = sub.Name;
            subject.Description = sub.Description;
            subject.IsDeleted = sub.IsDeleted;

            await _dataContext.SaveChangesAsync();
            return Ok();
        }


        [HttpDelete("deleteSubject/{id}")]
        public async Task<ActionResult<List<Subject>>> DeleteSubject(int id)
        {
            var subject = await _dataContext.Subject.FindAsync(id);
            if (subject == null)
            {
                return BadRequest("subject not found");
            }
            subject.IsDeleted = true;
            await _dataContext.SaveChangesAsync();
            return Ok();
        }
    }
}
