﻿using IOMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using IOMS.Data;

namespace IOMS.Controllers
{
    [Route("api/teachers")]
    [ApiController]
    public class TeacherController : Controller
    {
        private readonly DataContext _dataContext;
        public TeacherController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public class TeacherSubjectDelete
        {
            public int SubjectID
            {
                get; set;
            }

            public int UserID
            {
                get; set;
            }
        }

        [HttpGet("getTeachers")]
        public async Task<ActionResult<List<UserModel>>> Get()
        {
            var teacher = await _dataContext.User.Where(u => u.RoleID == 4 && u.isDeleted == false).ToListAsync();
            return Ok(teacher);
        }

        [HttpPost("uploadingFile")]
        public async Task<ActionResult<List<UploadFile>>> UploadingFileDoc(UploadFile file)
            {
            _dataContext.UploadFile.Add(file);
            await _dataContext.SaveChangesAsync();
            return Ok();
        }

        [HttpGet("uploadFile/{id}")]
        public async Task<ActionResult<List<UploadFile>>> GetFile(int id)
        {
            var SubjectFiles = await _dataContext.UploadFile.Where(f=>f.SubjectID==id).ToListAsync();
            return Ok(SubjectFiles  );
        }

        [HttpGet("getTeacher/{id}")]
        public async Task<ActionResult<List<Subject>>> GetByID(int id)
        {
            var teacher = await _dataContext.User.Where(u => u.RoleID == 4 && u.ID == id).FirstAsync();
            if (teacher == null)
            {
                return BadRequest("Teacher not found");
            }
            var SubjectListIDs = await _dataContext.UserIdSubject.Where(t => t.UserID == teacher.ID).ToListAsync();
            List<Subject> SubjectList = new List<Subject>();
            foreach (var UserSubject in SubjectListIDs)
            {
                Subject sub = new Subject();
                sub = await _dataContext.Subject.Where(s => s.Id == UserSubject.SubjectId).FirstAsync();
                SubjectList.Add(sub);
            }
            return Ok(SubjectList);
        }
        [HttpPost("DeleteTeacherSubject")]
        public async Task<ActionResult<List<Subject>>> DeleteByID(TeacherSubjectDelete IDs)
        {
            var subject = await _dataContext.UserIdSubject.Where(t => t.SubjectId == IDs.SubjectID && t.UserID ==IDs.UserID).FirstOrDefaultAsync();
            if (subject == null)
            {
                return BadRequest("Subject not found");
            }
            _dataContext.UserIdSubject.Remove(subject);
            await _dataContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("updateTeacher")]
        public async Task<ActionResult<List<UserModel>>> UpdateUser(UserAssignSubject UserSub)
        {
            var user = await _dataContext.User.FindAsync(UserSub.UserID);
            if (user == null)
            {
                return BadRequest("Teacher not found");
            }
            var UserSubjectList = await _dataContext.UserIdSubject.Where(u => u.UserID == UserSub.UserID).ToListAsync();
            foreach (var UserSubject in UserSubjectList)
            {
                _dataContext.UserIdSubject.Remove(UserSubject);
            }
            foreach (var item in UserSub.SubjectIds)
            {
                var userSub = new UserIdSubject();
                userSub.SubjectId = item;
                userSub.UserID = UserSub.UserID;
                userSub.Id = 0;
                _dataContext.UserIdSubject.Add(userSub);
                await _dataContext.SaveChangesAsync();
            }
            return Ok();
        }
    }
}
