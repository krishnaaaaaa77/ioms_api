﻿using IOMS.Data;
using IOMS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IOMS.Controllers
{
    [Route("api/Roles")]
    [ApiController]
    public class Roles : ControllerBase
    {

        private readonly DataContext _dataContext;
        public Roles(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        [HttpGet("getRoles")]
        public async Task<ActionResult<List<RolesModel>>> Get()
        {
            var role = await _dataContext.Roles.ToListAsync();
            return Ok(role);
        }

        [HttpGet("getRole/{id}")]
        public async Task<ActionResult<List<RolesModel>>> GetByID(int id)
        {
            var role = await _dataContext.Roles.FindAsync(id);
            if (role == null)
            {
                return BadRequest("Hero not found");
            }
            return Ok(role);
        }

        [HttpPost("addRole")]
        public async Task<ActionResult<List<RolesModel>>> AddRole(RolesModel role)
        {
            _dataContext.Roles.Add(role);
            await _dataContext.SaveChangesAsync();
            return Ok(await _dataContext.Roles.ToListAsync());
        }

        [HttpPatch("updateRole")]
        public async Task<ActionResult<List<RolesModel>>> UpdateRole(RolesModel roles)
        {
            var role = await _dataContext.Roles.FindAsync(roles.ID);
            if (role == null)
            {
                return BadRequest("Role not found");
            }
            role.Description = roles.Description;
            role.Name = roles.Name;

            await _dataContext.SaveChangesAsync();
            return Ok(await _dataContext.Roles.ToListAsync());
        }


        [HttpDelete("deleteRole/{id}")]
        public async Task<ActionResult<List<RolesModel>>> DeleteRole(int id)
        {
            var role = await _dataContext.Roles.FindAsync(id);
            if (role == null)
            {
                return BadRequest("Role not found");
            }
            _dataContext.Roles.Remove(role);
            await _dataContext.SaveChangesAsync();
            return Ok(await _dataContext.Roles.ToListAsync());
        }
    }
}
