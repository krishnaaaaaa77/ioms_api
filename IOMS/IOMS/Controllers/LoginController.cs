﻿using IOMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using IOMS.Data;

namespace IOMS.Controllers
{
    [Route("api/login")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly DataContext _dataContext;
        public LoginController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        [HttpPost]
        public async Task<ActionResult> login(LoginModel login)
        {
            UserModel user = await _dataContext.User.Where(u => u.Email == login.Email).FirstAsync();
            if (user == null)
                BadRequest();
            if (user.Password == login.Password)
            {
                return Ok(user);
            }
            return BadRequest();
        }
    }
}
